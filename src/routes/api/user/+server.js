import { json } from '@sveltejs/kit';
import path from 'path';
import Datastore from 'nedb-promises';
import { isUndefined } from '../../../lib/utility';

/** @param {(e: import('@sveltejs/kit').RequestEvent, send: { add(data, total), ok(), fail() }) } callback */
const handler = (callback) => {
  return async (e) => {
    const payload = { data: null, meta: { query: useQuery(e), ...useQueryPagination(e) } };
    const options = {
      add(data, total = 0) {
        payload.data = data;
        payload.meta.pageCount = Math.ceil(total / payload.meta.pageSize) || 0;
        payload.meta.count = data ? data.length ?? 1 : 0;
        payload.meta.length = total;
      },
      ok(data, total) {
        payload.success = true;
        options.add(data, total);
        return json(payload);
      },
      OK() {
        return json(payload);
      },
      fail(message) {
        payload.success = false;
        payload.message = message;
        return json(payload);
      },
    };
    try {
      return await callback(e, options);
    } catch (err) {
      return options.fail(err.message);
    }
  };
};

// const users = Datastore.create(path.resolve('./src/db/users.db'));
const users = Datastore.create({ inMemoryOnly: true });

export const GET = handler(async (e, send) => {
  const [start, limit] = usePagination(e);
  const rawQuery = useQuery(e);

  const { fullName } = rawQuery;
  if (fullName) {
    const regex = String(fullName)
      .replace(/[^a-zA-Z0-9]|\ /gi, '')
      .split('')
      .join('.*');
    rawQuery.fullName = new RegExp(regex, 'gi');
  }

  const query = { ...rawQuery, ...useQueryCheckDeleted() };
  const data = await users.find(query, {}).sort({ created_at: -1 }).skip(start).limit(limit);
  const total = await users.count(query);

  return send.ok(data, total);
});

export const POST = handler(async (e, send) => {
  try {
    // const payload = (await (await fetch('https://randomuser.me/api')).json())?.results?.[0];
    const payload = await e.request.json();
    if (payload) {
      if (await users.count({ username: payload.username })) {
        return send.fail('username has been used');
      }

      const time = new Date().toISOString();
      const result = { ...payload, created_at: time, updated_at: time };

      return send.ok(await users.insert(result), await useCount());
    }
  } catch (err) {}

  return send.fail('payload empty');
});

export const PUT = handler(async (e, send) => {
  try {
    const payload = await e.request.json();
    if (payload) {
      const query = { _id: payload._id, ...useQueryCheckDeleted() };
      const data = await users.findOne(query);

      if (data && !data.deleted_at) {
        const same = await users.count({
          $and: [{ username: payload.username }, { $not: { _id: payload._id } }],
        });

        if (same) return send.fail('username has been used');

        const time = new Date().toISOString();
        await users.update(query, { ...data, ...payload, updated_at: time }, { upsert: true });

        return send.ok(await users.findOne(query), await useCount());
      }

      return send.fail('data user not found');
    }
  } catch (err) {}

  return send.fail('payload empty');
});

export const DELETE = handler(async (e, send) => {
  try {
    const payload = await e.request.json();
    if (payload) {
      const query = { _id: payload._id, ...useQueryCheckDeleted() };
      const data = await users.findOne(query);

      if (data) {
        const time = new Date().toISOString();
        const payloadUser = { ...data, deleted_at: time };
        await users.update(query, payloadUser, { upsert: true });

        return send.ok(payloadUser, await useCount());
      }

      return send.fail('data user not found');
    }
  } catch (err) {}

  return send.fail('payload empty');
});

//
// HELPER
//

const useQueryCheckDeleted = () => ({ deleted_at: { $exists: false } });

const useCount = async () => users.count(useQueryCheckDeleted());

const useQuery = (e) => {
  try {
    const query = {};
    e.url.searchParams.forEach((val, key) => {
      if (key !== 'page' && key !== 'pageSize') {
        try {
          query[key] = JSON.parse(val);
        } catch (err) {
          query[key] = val;
        }
      }
    });
    return query;
  } catch (err) {}
  return {};
};

const useQueryPagination = (e) => {
  try {
    const page = Number(e.url.searchParams.get('page')) || 1;
    const pageSize = Number(e.url.searchParams.get('pageSize')) || DEFAULT_PAGESIZE;
    return { page, pageSize };
  } catch (err) {}
  return { page: 1, pageSize: DEFAULT_PAGESIZE };
};

const usePagination = (e) => {
  try {
    const { page, pageSize } = useQueryPagination(e);
    return [(page - 1) * pageSize, pageSize];
  } catch (err) {}
  return [0, DEFAULT_PAGESIZE];
};

//
// CONSTANT
//

const DEFAULT_PAGESIZE = 10;
