// @ts-nocheck

import { isArray, isString, isUndefined, toastMessage } from '../lib/utility';

function requestEndpoint(method, url, body, options) {
  const opt = {
    method,
    body,
    credentials: 'include',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
  };
  if (options) {
    for (const i in options) opt[i] = options[i];
  }

  let req = fetch(url, opt).then((response) => {
    if (response.ok) return response.json();
    return response.text().then((text) => {
      try {
        return JSON.parse(text);
      } catch (e) {
        return text;
      }
    });
  });

  if (import.meta.env.DEV) {
    req.then((payload) => {
      console.log(`fetch: ${url}`, payload);
      return payload;
    });
  }
  return req.then((payload) => {
    if (payload && !payload?.success) {
      toastMessage(`Gagal: [${payload.message}]`);
      throw payload.message;
    }
    return payload;
  });
}

function checkBody(data) {
  if (data instanceof FormData) return data;
  return JSON.stringify(data);
}

export const request = {
  get(url, options) {
    return (data, params) => {
      let endpoint = resolveUrl(url, params);
      if (data) endpoint += `?${resolveQueryParams(data)}`;

      return requestEndpoint('GET', endpoint, undefined, options);
    };
  },
  post(url, options) {
    return (data, params) => requestEndpoint('POST', resolveUrl(url, params), checkBody(data), options);
  },
  put(url, options) {
    return (data, params) => requestEndpoint('PUT', resolveUrl(url, params), checkBody(data), options);
  },
  delete(url, options) {
    return (data, params) => requestEndpoint('DELETE', resolveUrl(url, params), checkBody(data), options);
  },
  patch(url, options) {
    return (data, params) => requestEndpoint('PATCH', resolveUrl(url, params), checkBody(data), options);
  },
};

const resolveUrl = (url, params) => {
  if (isArray(params)) return `${url}/${params.join('/')}`;
  if (isString(params)) return `${url}/${params}`;
  return url;
};

export const resolveQueryParams = (query) => {
  if (typeof query !== 'object') return '';
  let result = '';
  let resolvedQuery = JSON.parse(JSON.stringify(query));
  Object.keys(resolvedQuery).map((key, i) => {
    const value = resolvedQuery[key];
    if (!isUndefined(value)) result += `${i ? '&' : ''}${key}=${value}`;
  });
  return result;
};
