import { request } from './config';

const endpoint = '/api/user';

export const getUsers = request.get(endpoint);
export const createUser = request.post(endpoint);
export const updateUser = request.put(endpoint);
export const deleteUser = request.delete(endpoint);
