import { toast } from '@zerodevx/svelte-toast';

export const generateSimpleUUID = () => '_' + Math.random().toString(24).slice(2, 12);

export const isUndefined = (value) => typeof value === 'undefined';
export const isNull = (value) => value === null;
export const isArray = (value) => Array.isArray(value);
export const isString = (value) => typeof value === 'string';
export const isNumber = (value) => typeof value === 'number';
export const isBoolean = (value) => typeof value === 'boolean';
export const isObject = (value) => typeof value === 'object';
export const isFn = (functionCallback) => typeof functionCallback === 'function';

export const toastMessage = (messages) => {
  toast.push(messages, {
    pausable: true,
    theme: {
      '--toastBarBackground': 'bg-pink',
      '--toastPadding': '0px 8px 6px !important',
      '--toastBorderRadius': '10px',
      //   '--toastBackground': '#fafafa',
      //   '--toastColor': 'grey.8700',
    },
  });
};
