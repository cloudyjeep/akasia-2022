import { afterUpdate, onDestroy, beforeUpdate, onMount } from 'svelte';
import { isFn } from './utility';

export function useEffect(cb, deps) {
  let cleanup;

  function apply() {
    if (isFn(cleanup)) cleanup();
    cleanup = cb();
  }

  if (deps) {
    let values = [];
    afterUpdate(() => {
      try {
        const new_values = deps();
        if (new_values.some((value, i) => value !== values[i])) {
          apply();
          values = new_values;
        }
      } catch (ee) {}
    });
  } else {
    // no deps = always run
    afterUpdate(apply);
  }

  onDestroy(() => {
    if (isFn(cleanup)) cleanup();
  });
}
